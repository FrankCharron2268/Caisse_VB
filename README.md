To Download: Click the cloud-looking button on the right of the screen

To launch project:
- Download or Pull Project.
- Open Caisse_VB_Build.
- Launch Projet _Final.exe.

To view solution:
- Download or Pull Project.
- Open Caisse_VB_Solution -> CaisseDepicerie_VB_Session1.
- Open Projet _Final.sln in your IDE or double click it.