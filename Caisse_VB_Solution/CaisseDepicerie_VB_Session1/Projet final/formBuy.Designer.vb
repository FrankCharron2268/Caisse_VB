﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class formBuy
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.itemname = New System.Windows.Forms.TextBox()
        Me.quantity = New System.Windows.Forms.TextBox()
        Me.z = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.butBuy = New System.Windows.Forms.Button()
        Me.butBack = New System.Windows.Forms.Button()
        Me.dgv1 = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.lblTotal = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.lbltest = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'itemname
        '
        Me.itemname.Location = New System.Drawing.Point(348, 268)
        Me.itemname.Name = "itemname"
        Me.itemname.Size = New System.Drawing.Size(163, 20)
        Me.itemname.TabIndex = 2
        Me.itemname.Text = "Pomme"
        '
        'quantity
        '
        Me.quantity.Location = New System.Drawing.Point(348, 336)
        Me.quantity.Name = "quantity"
        Me.quantity.Size = New System.Drawing.Size(130, 20)
        Me.quantity.TabIndex = 3
        Me.quantity.Text = "2"
        '
        'z
        '
        Me.z.AutoSize = True
        Me.z.Location = New System.Drawing.Point(224, 271)
        Me.z.Name = "z"
        Me.z.Size = New System.Drawing.Size(80, 13)
        Me.z.TabIndex = 4
        Me.z.Text = "Nom du Produit"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(224, 339)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Quantite produit"
        '
        'butBuy
        '
        Me.butBuy.Location = New System.Drawing.Point(227, 414)
        Me.butBuy.Name = "butBuy"
        Me.butBuy.Size = New System.Drawing.Size(152, 57)
        Me.butBuy.TabIndex = 8
        Me.butBuy.Text = "Buy"
        Me.butBuy.UseVisualStyleBackColor = True
        '
        'butBack
        '
        Me.butBack.Location = New System.Drawing.Point(442, 414)
        Me.butBack.Name = "butBack"
        Me.butBack.Size = New System.Drawing.Size(152, 57)
        Me.butBack.TabIndex = 9
        Me.butBack.Text = "Retour"
        Me.butBack.UseVisualStyleBackColor = True
        '
        'dgv1
        '
        Me.dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4})
        Me.dgv1.Location = New System.Drawing.Point(210, 47)
        Me.dgv1.Name = "dgv1"
        Me.dgv1.Size = New System.Drawing.Size(434, 189)
        Me.dgv1.TabIndex = 19
        '
        'Column1
        '
        Me.Column1.HeaderText = "Name"
        Me.Column1.Name = "Column1"
        '
        'Column2
        '
        Me.Column2.HeaderText = "description"
        Me.Column2.Name = "Column2"
        '
        'Column3
        '
        Me.Column3.HeaderText = "quantity"
        Me.Column3.Name = "Column3"
        '
        'Column4
        '
        Me.Column4.HeaderText = "price"
        Me.Column4.Name = "Column4"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(253, 378)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(74, 13)
        Me.Label2.TabIndex = 20
        Me.Label2.Text = "Cout par items"
        '
        'lblTotal
        '
        Me.lblTotal.AutoSize = True
        Me.lblTotal.Location = New System.Drawing.Point(396, 378)
        Me.lblTotal.Name = "lblTotal"
        Me.lblTotal.Size = New System.Drawing.Size(0, 13)
        Me.lblTotal.TabIndex = 21
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(227, 477)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(152, 47)
        Me.Button1.TabIndex = 22
        Me.Button1.Text = "Facture"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'lbltest
        '
        Me.lbltest.AutoSize = True
        Me.lbltest.Location = New System.Drawing.Point(518, 378)
        Me.lbltest.Name = "lbltest"
        Me.lbltest.Size = New System.Drawing.Size(0, 13)
        Me.lbltest.TabIndex = 23
        Me.lbltest.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(439, 378)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(72, 13)
        Me.Label3.TabIndex = 24
        Me.Label3.Text = "Facture totale"
        '
        'formBuy
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(860, 592)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lbltest)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lblTotal)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dgv1)
        Me.Controls.Add(Me.butBack)
        Me.Controls.Add(Me.butBuy)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.z)
        Me.Controls.Add(Me.quantity)
        Me.Controls.Add(Me.itemname)
        Me.Name = "formBuy"
        Me.Text = "formBuy"
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents itemname As TextBox
    Friend WithEvents quantity As TextBox
    Friend WithEvents z As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents butBuy As Button
    Friend WithEvents butBack As Button
    Friend WithEvents dgv1 As DataGridView
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents Label2 As Label
    Friend WithEvents lblTotal As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents lbltest As Label
    Friend WithEvents Label3 As Label
End Class
