﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class formAdmin
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.butView = New System.Windows.Forms.Button()
        Me.butBuy = New System.Windows.Forms.Button()
        Me.butBack = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'butView
        '
        Me.butView.Location = New System.Drawing.Point(236, 144)
        Me.butView.Name = "butView"
        Me.butView.Size = New System.Drawing.Size(152, 57)
        Me.butView.TabIndex = 3
        Me.butView.Text = "voir les produits"
        Me.butView.UseVisualStyleBackColor = True
        '
        'butBuy
        '
        Me.butBuy.Location = New System.Drawing.Point(461, 144)
        Me.butBuy.Name = "butBuy"
        Me.butBuy.Size = New System.Drawing.Size(152, 57)
        Me.butBuy.TabIndex = 4
        Me.butBuy.Text = "ajouter un produit"
        Me.butBuy.UseVisualStyleBackColor = True
        '
        'butBack
        '
        Me.butBack.Location = New System.Drawing.Point(461, 254)
        Me.butBack.Name = "butBack"
        Me.butBack.Size = New System.Drawing.Size(152, 57)
        Me.butBack.TabIndex = 5
        Me.butBack.Text = "Retour"
        Me.butBack.UseVisualStyleBackColor = True
        '
        'formAdmin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(860, 592)
        Me.Controls.Add(Me.butBack)
        Me.Controls.Add(Me.butBuy)
        Me.Controls.Add(Me.butView)
        Me.Name = "formAdmin"
        Me.Text = "formAdmin"
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents butView As Button
    Friend WithEvents butBuy As Button
    Friend WithEvents butBack As Button
End Class
