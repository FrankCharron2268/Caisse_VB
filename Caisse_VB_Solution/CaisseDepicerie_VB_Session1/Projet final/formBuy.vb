﻿Public Class formBuy

    Public Sub butBuy_Click(sender As Object, e As EventArgs) Handles butBuy.Click
        Dim soustotal As Double
        Dim multiplier As Integer
        Dim intRow As Integer
        Dim total As Double

        'les deux IF si dessous empeche le programme de planter si aucune valeure nest entre dans les txtbox
        If itemname.Text = Nothing Then
            MsgBox("Veuillez selectionner un item")
            Return
        End If
        If quantity.Text = Nothing Then
            MsgBox("Veuillez selectionner une quatité")
            Return
        End If
        multiplier = quantity.Text



        'loop les lignes a la recherche de la ligne correspondante et retourne si la quantite demander est superieur a celle en stock avec un message derreur
        For intRow = 0 To dgv1.Rows.Count - 1
            If itemname.Text = Me.dgv1.Rows(intRow).Cells(0).Value Then
                If multiplier > Me.dgv1.Rows(intRow).Cells(2).Value Then
                    MsgBox("quantite en stock insuffisante")
                    Return
                End If
            End If
        Next

        'loop les lignes a la recherche de la ligne correspondante
        For intRow = 0 To dgv1.Rows.Count - 1
            If itemname.Text = Me.dgv1.Rows(intRow).Cells(0).Value Then
                Me.dgv1.Rows(intRow).Cells(2).Value = CDbl(Me.dgv1.Rows(intRow).Cells(2).Value) - CDbl(quantity.Text)
                formView.dgv1.Rows(intRow).Cells(2).Value = CDbl(formView.dgv1.Rows(intRow).Cells(2).Value) - CDbl(quantity.Text) ' actualise le dgv de formView

                'calcul le prix de chaque achats individuels
                soustotal += CDbl(multiplier) * CDbl(Me.dgv1.Rows(intRow).Cells(3).Value)
                total = CDbl(multiplier) * CDbl(Me.dgv1.Rows(intRow).Cells(3).Value) + soustotal * (14.975 / 100)
                lblTotal.Text = Format(total, "c")
                'envoie le nom la quantite et le total de litem selectionner dans listachat
                listachat.dgv1.Rows.Add(CStr(itemname.Text), CStr(quantity.Text), CDbl(lblTotal.Text))
                quantity.Text = ""
                itemname.Text = ""
                Exit For
            End If
        Next intRow

        Dim totalx As String = 0
        For i As Integer = 0 To listachat.dgv1.RowCount - 1  'calcule e total des achats 
            totalx += listachat.dgv1.Rows(i).Cells(2).Value

        Next

        lbltest.Text = "$" + totalx
        lbltest.Visible = True

    End Sub


    Private Sub butBack_Click(sender As Object, e As EventArgs) Handles butBack.Click
        Hide()
        main.Show()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Hide()
        listachat.Show()
    End Sub

   
End Class
