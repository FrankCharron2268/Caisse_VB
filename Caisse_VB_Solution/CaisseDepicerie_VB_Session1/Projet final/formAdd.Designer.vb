﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class formAdd
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.butAdd = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.txtQty = New System.Windows.Forms.TextBox()
        Me.txtDesc = New System.Windows.Forms.TextBox()
        Me.txtprice = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(491, 356)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(152, 57)
        Me.Button4.TabIndex = 7
        Me.Button4.Text = "Retour"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'butAdd
        '
        Me.butAdd.Location = New System.Drawing.Point(57, 356)
        Me.butAdd.Name = "butAdd"
        Me.butAdd.Size = New System.Drawing.Size(152, 57)
        Me.butAdd.TabIndex = 8
        Me.butAdd.Text = "Add"
        Me.butAdd.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(45, 100)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(64, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Nom produit"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(45, 128)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 13)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "description"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(45, 159)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(45, 13)
        Me.Label4.TabIndex = 12
        Me.Label4.Text = "quantite"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(45, 190)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(23, 13)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "prix"
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(109, 97)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(209, 20)
        Me.txtName.TabIndex = 14
        Me.txtName.Text = "Pomme"
        '
        'txtQty
        '
        Me.txtQty.Location = New System.Drawing.Point(109, 156)
        Me.txtQty.Name = "txtQty"
        Me.txtQty.Size = New System.Drawing.Size(209, 20)
        Me.txtQty.TabIndex = 15
        Me.txtQty.Text = "10"
        '
        'txtDesc
        '
        Me.txtDesc.Location = New System.Drawing.Point(109, 125)
        Me.txtDesc.Name = "txtDesc"
        Me.txtDesc.Size = New System.Drawing.Size(209, 20)
        Me.txtDesc.TabIndex = 16
        Me.txtDesc.Text = "Rouge"
        '
        'txtprice
        '
        Me.txtprice.Location = New System.Drawing.Point(109, 187)
        Me.txtprice.Name = "txtprice"
        Me.txtprice.Size = New System.Drawing.Size(209, 20)
        Me.txtprice.TabIndex = 17
        Me.txtprice.Text = "2.50"
        '
        'formAdd
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(860, 592)
        Me.Controls.Add(Me.txtprice)
        Me.Controls.Add(Me.txtDesc)
        Me.Controls.Add(Me.txtQty)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.butAdd)
        Me.Controls.Add(Me.Button4)
        Me.Name = "formAdd"
        Me.Text = "formBuy"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button4 As Button
    Friend WithEvents butAdd As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents txtName As TextBox
    Friend WithEvents txtQty As TextBox
    Friend WithEvents txtDesc As TextBox
    Friend WithEvents txtprice As TextBox
End Class
